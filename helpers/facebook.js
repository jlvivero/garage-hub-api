var face = require('fb');
var mongoose = require('mongoose');
var Promise = require('promise');
var Hardware = require('../models/hardware');
var Session = require('../models/session');
var link;


var access_token = new Promise(function(fulfill,reject){
  var session = {}
  Session.findOne({sessionName:'test'},function(err,sess){
    if(sess){
      session.client_id = sess.id;
      session.client_secret = sess.secret;
      face.api('oauth/access_token',{
        client_id: session.client_id,
        client_secret: session.client_secret,
        grant_type: 'client_credentials'}, function(res){
          if(!res || res.error){
            console.log(!res ? 'error occurred' : res.error);
            reject(res.error);
          }
          else {
            console.log(res.access_token);
            fulfill(res.access_token);
          }
        });
    }
    else{
      //default values, should be changed to garage stuff
      session.client_id = '214452038941667';
      session.client_secret = '2f20bd63078928880bbcd123a349923d';
      face.api('oauth/access_token',{
        client_id: session.client_id,
        client_secret: session.client_secret,
        grant_type: 'client_credentials'}, function(res){
          if(!res || res.error){
            console.log(!res ? 'error occurred' : res.error);
            reject(res.error);
          }
          else {
            console.log(res.access_token);
            fulfill(res.access_token);
          }
        });
    }
  });
});

var facebook = setInterval(function(){
  access_token.then(function(value){
    face.setAccessToken(value);
    Session.findOne({sessionName:'test'}, function(err,s){
      if(s){
        link = s.link;
        face.api(link,'get',{"fields":"link,name,fan_count,checkins,new_like_count"},function(res){
          if(!res || res.error){
            console.log(!res ? 'error occurred' : res.error);
            return;
          }
          else {
            //for testing im just gonna print the info and see if it works
            console.log("id:");
            console.log(res.id);
            console.log("name:");
            console.log(res.name);
            console.log("fan count:");
            console.log(res.fan_count);
            console.log("checkins:");
            console.log(res.checkins);
            console.log("link:");
            console.log(res.link);
            Hardware.findOne({id: res.id}, function(err,hardware){
              if(!hardware){
                console.log("new hardware data");
                var hard = new Hardware({id: res.id, name: 'test', fan_count: res.fan_count, checkins: res.checkins, link: res.link});
                hard.save(function(error, h){
                  if(error){console.log(error);}
                  console.log("I saved the new hardware!");
                });
              }
              else{
                hardware.id = res.id || hardware.id;
                hardware.name = 'test'
                hardware.fan_count = res.fan_count || hardware.fan_count;
                hardware.checkins = res.checkins || hardware.checkins;
                hardware.link = res.link || hardware.link;
                hardware.save(function(err){
                  if(err){
                    console.log(err);
                  }
                  else{
                    console.log("modified the hardware data");
                  }
                });
              }
            });
          }
        });
      }
      else{
        //if no link is provided it will always default to garage hub page
        link = 'https://www.facebook.com/elgarageprojecthub/';
        face.api(link,'get',{"fields":"link,name,fan_count,checkins,new_like_count"},function(res){
          if(!res || res.error){
            console.log(!res ? 'error occurred' : res.error);
            return;
          }
          else {
            //for testing im just gonna print the info and see if it works
            console.log("id:");
            console.log(res.id);
            console.log("name:");
            console.log(res.name);
            console.log("fan count:");
            console.log(res.fan_count);
            console.log("checkins:");
            console.log(res.checkins);
            console.log("link:");
            console.log(res.link);
            Hardware.findOne({id: res.id}, function(err,hardware){
              if(!hardware){
                console.log("new hardware data");
                var hard = new Hardware({id: res.id, name: 'test', fan_count: res.fan_count, checkins: res.checkins, link: res.link});
                hard.save(function(error, h){
                  if(error){console.log(error);}
                    console.log("I saved the new hardware!");
                });
              }
              else{
                hardware.id = res.id || hardware.id;
                hardware.name = 'test'
                hardware.fan_count = res.fan_count || hardware.fan_count;
                hardware.checkins = res.checkins || hardware.checkins;
                hardware.link = res.link || hardware.link;
                console.log("hardware link is:");
                console.log(hardware.link);
                hardware.save(function(err){
                  if(err){
                    console.log(err);
                  }
                  else{
                    console.log("modified hardware data");
                  }
                });
              }
            });
          }
        })
      }
    });
  });
},10000); //this is 5 minutes 300000

module.exports = facebook;
