var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SessionSchema = new Schema({
  sessionName: String,
  token: String,
  id: String,
  secret: String,
  max_likes: Number,
  link: String
});

module.exports = mongoose.model('Session',SessionSchema);
