var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var HardwareSchema = new Schema({
  name: String,
  id: String,
  fan_count: Number,
  checkins: Number,
  link: String
});

module.exports = mongoose.model('Hardware',HardwareSchema);
