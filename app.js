var fs = require('fs');
var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');
var mongoose = require('mongoose');
var facebook = require('./helpers/facebook');
var dashboard = require('./helpers/dashboard');
var app = module.exports.app = exports.app = express();

mongoose.connect("mongodb://localhost/test", function (err) {
  if(err){
    console.log(err);
  } else {
    console.log('mongo connected');
  }
});

app.use('/', express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(function(req,res,next){
  // Set permissive CORS header - this allows this server to be used only as
  // an API server in conjunction with something like webpack-dev-server.
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Disable caching so we'll always get the latest comments.
  res.setHeader('Cache-Control', 'no-cache');
  next();
});

//routes.initialize(app);
app.use('/', require('./routes'));

var apiFeed = function(){
  facebook();
  dashboard();
}
//app.listen(process.env.PORT || 3000);
//app.listen(4000, "0.0.0.0", function (err) {
app.listen(3000, 'localhost', function (err) {
  if (err) {
    console.log(err);
    return;
  }
  console.log("listening to port:   " + 4000);
});
