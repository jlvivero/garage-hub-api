 var Complete = React.createClass({
   getInitialState: function(){
     return {page: 0}
   },
   onButtonClick: function(value){
     this.setState({
       page: value
     });
   },
   render: function(){
     return(
       <div className="Complete">
         <Header onButtonClick={this.onButtonClick}/>
         <Body page={this.state.page}/>
         <Footer/>
       </div>
     );
   }
 });

//TODO: need to change the css of the buttons so that it's visible in the white screen
var Header = React.createClass({
  goHome: function(){
    this.props.onButtonClick(0);
  },
  goSession: function(){
    this.props.onButtonClick(1);
  },
  goGoals: function(){
    this.props.onButtonClick(2);
  },
  render: function(){
    return(
      <div className="Header">
        <section className="page-nav">
          <section className="btnl">
            <button className="btn" onClick={this.goHome}>Home</button>
          </section>
          <section className="btnr">
            <button className="btn" onClick={this.goSession}>Session</button>
            <button className="btn" onClick={this.goGoals}>How to</button>
          </section>
        </section>
        <section className="page-header">
          <h1 className="project-name">Physical Dashboard</h1>
          <h2 className="project-tagline">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean hendrerit velit iaculis sodales fringilla. In libero ligula, efficitur id sapien vel, fringilla ornare turpis. Nulla aliquam odio vitae odio dapibus posuere. Suspendisse sagittis turpis id lorem aliquam, dignissim malesuada dolor mattis. Nunc consequat, nulla sed aliquam eleifend, elit arcu dictum enim, et dignissim massa erat at libero. Phasellus molestie leo eget arcu lacinia, a maximus neque malesuada. Sed aliquam non justo a tristique. </h2>
        </section>
      </div>
    );
  }
});

var Body = React.createClass({
  submitSession: function(){
    var sendString = "id=" + this.refs.fbid.value + "&secret=" + this.refs.fbsecret.value + "&link=" + this.refs.pagelink.value;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open('POST', 'http://localhost:3000/facebook/newsession', true);
    xmlhttp.setRequestHeader('content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send(sendString);
    return;
  },
  renderHome: function(){
    return(
      <div className="Body">
        <section className="main-content">
          <h1>
            <a id="Instructions" className="anchor" href="#Instructions" aria-hidden="true"><span aria-hidden="true" className="ref-link"></span></a>Instrucciones</h1>

          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean hendrerit velit iaculis sodales fringilla. In libero ligula, efficitur id sapien vel, fringilla ornare turpis. Nulla aliquam odio vitae odio dapibus posuere.</p>

          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </section>
      </div>
    );
  },
  renderSession: function(){
    return(
      <div className="Body">
        <section className="main-content">
          <h1>
            <a id="Session" className="anchor" href="#Session" aria-hidden="true"><span aria-hidden="true" className="ref-link"></span></a>Session</h1>
          <p>Fill out this form with your facebook developer information and the link you wish to obtain information from</p>
          <p>facebook ID </p>
          <input type="text" ref="fbid" />
          <br></br>
          <p>facebook secret </p>
          <input type="text" ref="fbsecret" />
          <br></br>
          <p>page link e.g. (https://www.facebook.com/elgarageprojecthub/) </p>
          <input type="text" ref="pagelink"/>
          <br></br><br></br>
          <button className="btn2" onClick={this.submitSession}>submit</button>
        </section>
      </div>
    );
  },
  renderGoals: function(){
    return(
      <div className="Body">
        <section className="main-content">
          <h1>
            <a id="Goals" className="anchor" href="#Goals" aria-hidden="true"><span aria-hidden="true" className="ref-link"></span></a>How to</h1>
          <p>To sign in follow this instruction:</p>
          <p>First go to  <a href="https://developers.facebook.com/">facebook developers page</a></p>
          <br></br>
          <img src="/resources/facebook1.png" className="tutorial"/>
          <p>Now sign in with your facebook account like so:</p>
          <br></br>
          <img src="/resources/facebook2.png" className="tutorial"/>
          <p>Click on the create app button, and we will start making our fan page app</p>
          <img src="/resources/facebook3.png" className="tutorial"/>
          <p>Then fill the information with the display name you want and your email, and choose the apps for pages category</p>
          <img src="/resources/facebook4.png" className="tutorial"/>
          <p>After you finished making the app click on my apps at the top right part of your page and click on your app</p>
          <img src="/resources/facebook5.png" className="tutorial"/>
          <p>Once you are in your app page you can locate your appID and your app Secrete (click show on app secret)</p>
          <p>Use this to fill out the Session tab and be able to activate your social media counter! </p>
          <img src="/resources/facebook6.png" className="tutorial"/>
        </section>
      </div>
    )
  },
  render: function(){
    if(this.props.page === 0){
      return this.renderHome();
    }
    else{
      if(this.props.page === 1){
        return this.renderSession();
      }
      else{
        if(this.props.page === 2){
          return this.renderGoals();
        }
        else{
          return this.renderHome();
        }
      }
    }
  }
});

var Footer = React.createClass({
  render: function(){
    return(
      <div className="Footer">
      <br></br><br></br>
        <div className="made">
          <p>Made by <a href="https://github.com/jlvivero/">Jose Luis Vivero</a> (<a href="https://twitter.com/jltaco">@jltaco</a>)</p>
        </div>
        <a href="http://elgaragehub.com">
          <img src="/resources/Logo.jpg" className="picture"/>
        </a>
      </div>
    );
  }
});

ReactDOM.render(<Complete />, document.getElementById('content'));
