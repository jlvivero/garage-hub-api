var mongoose = require('mongoose');
var Promise = require('promise');
var Session = require('../models/session');

var links = function(req,res){
  var max = req.params.link;
  Session.findOne({sessionName:'test'},function(err, session){
    if(!session){
      console.log(err);
      res.sendStatus(404);
    }
    else{
      session.link = max || session.link;
      session.save(function(err){
        if(err){
          return err;
        }
        else{
          res.sendStatus(200);
        }
      });
    }
  });
}

module.exports = links;
