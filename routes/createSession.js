var request = require('request');
var mongoose = require('mongoose');
var Session = require('../models/session');

var createSession = function(req,res){
  console.log("getting here")
  Session.findOne({sessionName:"test"}, function(err,sess){
    if(sess){
      console.log(req.body.link);
      sess.id = req.body.id || sess.id;
      sess.secret = req.body.secret || sess.secret;
      sess.link = req.body.link || sess.link;
      sess.save(function(err){
        if(err){
          res.send(err);
        }
        else{
          console.log("it saved");
          res.sendStatus(200);
        }
      });
    }
    else{
      var session = new Session({sessionName:"test", id: req.body.id, secret: req.body.secret, link:req.body.link, max_likes: 1000});
      session.save(function(err,ses){
        if(err){
          console.log(err);
          res.send(err);
        }
        else{
          res.sendStatus(200);
        }
      });
    }
  });
}

module.exports = createSession;
