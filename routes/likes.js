var mongoose = require('mongoose');
var Promise = require('promise');
var Session = require('../models/session');

var max_likes = function(req,res){
  var max = req.body.goal;
  console.log(req.body.goal);
  console.log(max);
  Session.findOne({sessionName:'test'},function(err, session){
    if(!session){
      console.log(err);
      res.sendStatus(404);
    }
    else{
      session.max_likes = max || session.max_likes;
      console.log(session.max_likes);
      session.save(function(err){
        if(err){
          return err;
        }
        else{
          res.sendStatus(200);
        }
      });
    }
  });
}

module.exports = max_likes;
